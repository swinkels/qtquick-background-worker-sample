#ifndef LISTVIEWMODEL_H
#define LISTVIEWMODEL_H

#include <QAbstractItemModel>
#include <QDateTime>
#include <QFutureWatcher>
#include <QList>
#include <QTest>
#include <QtConcurrent>

#include <functional>
#include <iostream>

class TaskInfo
{
public:

    explicit TaskInfo(int index):
        mIndex(index), mDispatchTime(QDateTime::currentDateTime())
    {
    }

    QFuture<void> runAsync()
    {
        auto f = std::bind(&TaskInfo::run, this);

        return QtConcurrent::run(f);
    }

    void run()
    {
        QTest::qSleep(4000);
    }

    int getIndex() const
    {
        return mIndex;
    }

    QDateTime getDispatchTime() const
    {
        return mDispatchTime;
    }

    QDateTime getStartTime() const
    {
        return mStartTime;
    }

    void setStartTime(const QDateTime& dateTime)
    {
        mStartTime = dateTime;
    }

    QDateTime getFinishTime() const
    {
        return mFinishTime;
    }

    void setFinishTime(const QDateTime& dateTime)
    {
        mFinishTime = dateTime;
    }

private:

    int mIndex;

    QDateTime mDispatchTime;
    QDateTime mStartTime;
    QDateTime mFinishTime;
};

class ListViewModel: public QAbstractListModel
{
    Q_OBJECT

public:

    enum ListViewModelRoles
    {
        IndexRole = Qt::UserRole + 1,
        DispatchTimeRole,
        StartTimeRole,
        FinishTimeRole
    };

    QHash<int, QByteArray> roleNames() const {
        QHash<int, QByteArray> roles;
        roles[IndexRole] = "index";
        roles[DispatchTimeRole] = "dispatchTime";
        roles[StartTimeRole] = "startTime";
        roles[FinishTimeRole] = "finishTime";
        return roles;
    }

    Q_INVOKABLE void submitTask()
    {
        int currentSize = mTaskInfos.size();
        TaskInfo info(currentSize);
        beginInsertRows(QModelIndex(), currentSize, currentSize);
        mTaskInfos.append(info);
        endInsertRows();

        mFutureWatchers.append(new QFutureWatcher<void>());
        QFutureWatcher<void>* pWatcher = mFutureWatchers.last();

        using namespace std::placeholders;

        connect(pWatcher, &QFutureWatcher<void>::finished, this, [=]{ handleFinishedTask(currentSize); });

        QFuture<void> future = mTaskInfos.last().runAsync();
        pWatcher->setFuture(future);
    }

    int columnCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return 4;
    }

    int rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return mTaskInfos.size();
    }

    QVariant data(const QModelIndex &index, int role) const
    {
        qDebug("ListViewModel::data index.row()=%d role=%d", index.row(), role);

        QVariant value;

        int row = index.row();
        TaskInfo info = mTaskInfos.at(row);

        switch (role)
        {
        case IndexRole:
            value = info.getIndex();
            break;
        case DispatchTimeRole:
            value = info.getDispatchTime().toString();
            break;
        case StartTimeRole:
            value = info.getStartTime().toString();
            break;
        case FinishTimeRole:
            value = info.getFinishTime().toString();
            break;
        default:
            break;
        }

        return value;
    }

protected slots:

    void handleStartedTask(int index, QDateTime dateTime)
    {
        mTaskInfos[index].setStartTime(dateTime);

        QModelIndex modelIndex = createIndex(index, 0);
        emit dataChanged(modelIndex, modelIndex);
    }

    void handleFinishedTask(int index)
    {
        qDebug("ListViewModel::handleFinishedTask(%d)", index);
        mTaskInfos[index].setFinishTime(QDateTime::currentDateTime());

        QModelIndex modelIndex = createIndex(index, 0);
        emit dataChanged(modelIndex, modelIndex);

        // the sender is a QFutureWatcher that we have to delete
        sender()->deleteLater();
    }

private:

    QList<TaskInfo> mTaskInfos;
    QList<QFutureWatcher<void> *> mFutureWatchers;
};

#endif // LISTVIEWMODEL_H
