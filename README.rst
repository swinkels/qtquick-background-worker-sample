Introduction
------------

This repo contains the source to a GUI application that dispatches tasks to
background threads and reports when each task has completed. The application is
build using QtQuick_ and QtConcurrent_, both part of Qt_.

Qt is a framework to build "UIs and applications that run anywhere on any
device, on any operating system". QtQuick is the part "designed to [...]
create [..] intuitive, modern, fluid user interfaces that are [...]  used on
mobile phones, media players, set-top boxes and other portable
devices". QtConcurrent provides "high-level [C++] APIs [...] to write
multi-threaded programs without using low-level threading primitives such as
mutexes, read-write locks, wait conditions, or semaphores".

The initial reason to build this application was to learn more about QtQuick
development using QtCreator_, the IDE specifically built to develop Qt
applications.

Prerequisites
-------------

The sample code has been developed and tested under Ubuntu 15.10 using Qt 5.5.

Getting started
---------------

To build the code, first clone it from Bitbucket::

  $> git clone https://swinkels@bitbucket.org/swinkels/qtquick-background-worker-sample

When you ssh access to Bitbucket, you can clone the repo as follows::

  $> git clone git@bitbucket.org:swinkels/qtquick-background-worker-sample.git

This creates a copy of the repo in subdirectory
qtquick-background-worker-sample.

Build from the command-line
---------------------------

The root of the repo contains a Makefile to create debug and release builds in
out-of-source build directory build/::

  $> cd qt-background-worker-sample/
  $> make

When make completes successfully, you can find the builds in directories
build/debug and build/release respectively. To run the application, which is
named \`lists`, go to the appropriate directory and run it::

  $> cd build/debug
  $> ./lists

Build using QtCreator
---------------------

To create a build using QtCreator

  1. Open QtCreator.
  2. Select menu option *File | Open File or Project...* to open a file dialog.
  3. Select lists.pro and QtCreator will show the Configure Project window.
  4. Select the defaults in the Configure Project window by pressing the
     *Configure Project* button.
  4. Select menu option *Build | Build All*.

This will create the debug build in the out-of-source build directory
build-lists-Desktop_Qt_5_5_1_GCC_64bit-Debug, which you can find in the parent
directory of the local checkout of this repository. The exact naming of this
directory might differ for your installation.

To run the executable, select *Build | Run*. By default, this will run the Debug
version.

.. _Qt: http://www.qt.io/
.. _QtQuick: http://doc.qt.io/qt-5/qtquick-index.html
.. _QtConcurrent: http://doc.qt.io/qt-5/qtconcurrent-index.html
.. _QtCreator: http://www.qt.io/ide/.
