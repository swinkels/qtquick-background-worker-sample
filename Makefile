BUILD_TYPES = debug release

# to create the build directory names, we prepend each build type with the
# directory that is the root of the out-of-source build directories
BUILD_DIRS = $(addprefix build/, $(BUILD_TYPES))

all: $(BUILD_DIRS)

$(BUILD_DIRS):
	@echo Create out-of-source build directory '$@'
	@mkdir -p $@
	cd $@ ; qmake CONFIG+="$(notdir $@)" ../.. ; make
	echo Done.
