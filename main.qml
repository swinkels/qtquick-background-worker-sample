import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.2

import MyModels 1.0

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("&Open")
                onTriggered: console.log("Open action triggered");
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }

    ColumnLayout {

        anchors.fill: parent
        Button {
            id: submitButton
            text: "Submit"
            onClicked: {
                 listViewModel.submitTask();
            }
        }

        TableView {
            id: tableView
            Layout.fillHeight: true
            Layout.fillWidth: true


            TableViewColumn {
                id: indexColumn
                title: "Index"
                role: "index"
            }

            TableViewColumn {
                id: dispatchTimeColumn
                title: "Dispatch time"
                role: "dispatchTime"
            }

            TableViewColumn {
                id: startTimeColumn
                title: "Start time"
                role: "startTime"
            }

            TableViewColumn {
                id: finishTimeColumn
                title: "Finish time"
                role: "finishTime"
            }

            model: ListViewModel {
                id: listViewModel
            }
        }
    }
}
