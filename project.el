(add-hook 'c++-mode-hook
    (lambda () (setq flycheck-gcc-include-path
        (list (expand-file-name "~/Qt/5.5/gcc_64/include/QtCore")))))
