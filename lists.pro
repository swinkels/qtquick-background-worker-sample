TEMPLATE = app

QT += concurrent qml quick testlib widgets

CONFIG += c++11

HEADERS += listviewmodel.h
SOURCES += main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

