#include "listviewmodel.h"

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlEngine>

int main(int argc, char *argv[])
{
    qSetMessagePattern("%{time} - %{type} - %{message}");

    QApplication app(argc, argv);

    qmlRegisterType<ListViewModel>("MyModels", 1, 0, "ListViewModel");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
